package br.dev.hygino.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "folowers", schema = "TSHARE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Follower {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "folowers_sq")
	@SequenceGenerator(schema = "TSHARE", name = "folowers_sq", sequenceName = "folowers_sq", initialValue = 1, allocationSize = 1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "folowers_id")
	private User follower;

	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;
}
